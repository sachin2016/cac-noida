package com.example.sachin007.cacnoida;

import android.graphics.Color;
import android.media.effect.Effect;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sachin007.cacnoida.Fragments.AboutUs;
import com.example.sachin007.cacnoida.Fragments.ContactUs;
import com.example.sachin007.cacnoida.Fragments.Courses;
import com.example.sachin007.cacnoida.Fragments.Enquiry;
import com.example.sachin007.cacnoida.Fragments.Events;
import com.example.sachin007.cacnoida.Fragments.Home;
import com.example.sachin007.cacnoida.Fragments.OnlineRegistration;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

import java.io.IOException;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.validation.Validator;

public class Dashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Fragment fragment;
    private Toolbar toolbar;
    private Effectstype effect;
    private EditText et_name, et_contact_number, et_technology;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#D89720"));
        }
        setContentView(R.layout.activity_dashboard);

        findViewById();
        setSupportActionBar(toolbar);
        dialogShow();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        if (savedInstanceState == null) {
            fragment = new Home();
            setFragment(fragment, "Home");
        }

    }

    public void findViewById() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

    }

    public void dialogShow() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);
        effect = Effectstype.SlideBottom;
        dialogBuilder
                .withTitle("Enquiry For...")
                .withTitleColor("#FFFFFF")
                .withDialogColor("#FFA500")
                .withDuration(700)
                .withEffect(effect)
                .withButton1Text("Send")
                .withButton2Text("Cancel")
                .setCustomView(R.layout.custom_view, getApplicationContext())
                .setButton1Click(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        EditText et_name = (EditText) dialogBuilder.findViewById(R.id.et_name);
                        EditText et_contact_number = (EditText) dialogBuilder.findViewById(R.id.et_contact_number);
                        EditText et_technology = (EditText) dialogBuilder.findViewById(R.id.et_technology);

                        try {
                            if (et_name.getText().toString().isEmpty()) {
                                et_name.setError("Please Enter Name");
                            } else if (et_contact_number.getText().toString().length() != 10) {
                                et_name.setError("Please Enter Valid Mobile Number");
                            } else if (et_technology.getText().toString().isEmpty()) {
                                et_name.setError("Please Enter Technology Name");
                            } else {
                                dialogBuilder.dismiss();
                                Toast.makeText(v.getContext(), "Submitted...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                })
                .show();

    }

    private void setFragment(Fragment fragment, String frg_name) {
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_replace, fragment, frg_name).commit();
        toolbar.setTitle(frg_name);
        this.fragment = fragment;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            fragment = new Home();
            setFragment(fragment, "Home");

        } else if (id == R.id.nav_aboutus) {
            fragment = new AboutUs();
            setFragment(fragment, "AboutUS");

        } else if (id == R.id.nav_course) {
            fragment = new Courses();
            setFragment(fragment, "Courses");

        } else if (id == R.id.nav_events) {
            fragment = new Events();
            setFragment(fragment, "Events");

        } else if (id == R.id.nav_onlineRrgistration) {
            fragment = new OnlineRegistration();
            setFragment(fragment, "OnlineRegistration");

        } else if (id == R.id.nav_contactus) {
            fragment = new ContactUs();
            setFragment(fragment, "ContactUs");

        } else if (id == R.id.nav_enquiry) {
            fragment = new Enquiry();
            setFragment(fragment, "Enquiry");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
