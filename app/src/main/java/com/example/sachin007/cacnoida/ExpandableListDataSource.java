package com.example.sachin007.cacnoida;

import android.content.Context;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by msahakyan on 22/10/15.
 */
public class ExpandableListDataSource {

    public static Map<String, List<String>> getData(Context context) {

        Map<String, List<String>> expandableListData = new TreeMap<>();

        List<String> dotnet = Arrays.asList(context.getResources().getStringArray(R.array.dotnet));
        List<String> Basicdotnet = Arrays.asList(context.getResources().getStringArray(R.array.Basicdotnet));
        List<String> AdvancedotNet = Arrays.asList(context.getResources().getStringArray(R.array.AdvancedotNet));
        List<String> Mvc3Mvc4Mvc5 = Arrays.asList(context.getResources().getStringArray(R.array.Mvc3Mvc4Mvc5));
        List<String> AngularjsandNodejs = Arrays.asList(context.getResources().getStringArray(R.array.AngularjsandNodejs));
        List<String> SharedpointServer2010 = Arrays.asList(context.getResources().getStringArray(R.array.SharedpointServer2010));


        expandableListData.put(dotnet.get(0),Basicdotnet);
        expandableListData.put(dotnet.get(1),AdvancedotNet);
        expandableListData.put(dotnet.get(2),Mvc3Mvc4Mvc5);
        expandableListData.put(dotnet.get(3),AngularjsandNodejs);
        expandableListData.put(dotnet.get(4),SharedpointServer2010);


        return expandableListData;
    }
}