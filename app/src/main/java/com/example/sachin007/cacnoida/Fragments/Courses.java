package com.example.sachin007.cacnoida.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.example.sachin007.cacnoida.CustomExpandableListAdapter;
import com.example.sachin007.cacnoida.ExpandableListDataSource;
import com.example.sachin007.cacnoida.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Courses extends Fragment {

    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;
    private ExpandableListView navList;
    private ExpandableListAdapter mExpandableListAdapter;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_courses, container, false);
        navList = (ExpandableListView)view.findViewById(R.id.navList);
        mExpandableListData = ExpandableListDataSource.getData(getActivity());
        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());
        mExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), mExpandableListTitle, mExpandableListData);
        navList.setAdapter(mExpandableListAdapter);
        navList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
            }
        });

        navList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        navList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String selectedItem = ((List) (mExpandableListData.get(mExpandableListTitle.get(groupPosition)))).get(childPosition).toString();

                return false;
            }
        });
        return  view;

    }


}
