package com.example.sachin007.cacnoida.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sachin007.cacnoida.R;

/**
 * Created by sachin007 on 27-09-2016.
 */

public class Enquiry extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.enquiry,container,false);
    }
}
