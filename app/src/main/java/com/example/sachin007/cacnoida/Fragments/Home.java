package com.example.sachin007.cacnoida.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.sachin007.cacnoida.R;

import java.util.HashMap;

public class Home extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout slider,compslider;
    private View view;
    private ImageView img_demo, iv_facebook;
    private TextView tv_see_more_training_courses, tv_training_courses_data, tv_see_more_corporate_training,
            tv_corporate_training_data, tv_see_more_communicationskills, tv_communicationskills_data,
            tv_see_more_campus_training, tv_campus_training_data;


    public Home() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, null);
        findViewById();

        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal", R.drawable.images);
        file_maps.put("Big Bang Theory", R.mipmap.sandeepkaran3);
        file_maps.put("House of Cards", R.mipmap.sandeepkaran1);
        file_maps.put("Game of Thrones", R.mipmap.sandeepkaran2);

        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView.image(file_maps.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.addOnPageChangeListener(this);

        HashMap<String, Integer> file_maps1 = new HashMap<String, Integer>();
        file_maps1.put("Hannibal", R.drawable.images);
        file_maps1.put("Big Bang Theory", R.mipmap.sandeepkaran3);
        file_maps1.put("House of Cards", R.mipmap.sandeepkaran1);
        file_maps1.put("Game of Thrones", R.mipmap.sandeepkaran2);

        for (String name : file_maps1.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView.image(file_maps1.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);
            compslider.addSlider(textSliderView);
        }
        compslider.setPresetTransformer(SliderLayout.Transformer.Foreground2Background);
        compslider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        compslider.setCustomAnimation(new DescriptionAnimation());
        compslider.setDuration(3000);
        compslider.addOnPageChangeListener(this);

        /*final int[] imageArray = {R.drawable.images, R.drawable.summer, R.mipmap.sandeepkaran1, R.mipmap.sandeepkaran2, R.mipmap.sandeepkaran3};
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            int i = 0;

            public void run() {
                img_demo.setImageResource(imageArray[i]);
                i++;
                if (i > imageArray.length - 1) {
                    i = 0;
                }
                handler.postDelayed(this, 2000);
            }
        };
        handler.postDelayed(runnable, 2000);*/

        tv_see_more_training_courses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_training_courses_data.getVisibility() == View.GONE) {
                    tv_training_courses_data.setVisibility(View.VISIBLE);
                } else {
                    tv_training_courses_data.setVisibility(View.GONE);
                }
            }
        });

        tv_see_more_corporate_training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_corporate_training_data.getVisibility() == View.GONE) {
                    tv_corporate_training_data.setVisibility(View.VISIBLE);
                } else {
                    tv_corporate_training_data.setVisibility(View.GONE);
                }
            }
        });

        tv_see_more_communicationskills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_communicationskills_data.getVisibility() == View.GONE) {
                    tv_communicationskills_data.setVisibility(View.VISIBLE);
                } else {
                    tv_communicationskills_data.setVisibility(View.GONE);
                }
            }
        });

        tv_see_more_campus_training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_campus_training_data.getVisibility() == View.GONE) {
                    tv_campus_training_data.setVisibility(View.VISIBLE);
                } else {
                    tv_campus_training_data.setVisibility(View.GONE);
                }
            }
        });
        iv_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/sandeep.karan.9?fref=ts"));
                    startActivity(intent);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/appetizerandroid")));
                }
            }
        });

        return view;
    }

    public void findViewById() {
        slider = (SliderLayout) view.findViewById(R.id.slider);
        compslider=(SliderLayout)view.findViewById(R.id.compslider);
        img_demo = (ImageView) view.findViewById(R.id.img_demo);
        iv_facebook = (ImageView) view.findViewById(R.id.iv_facebook);
        tv_see_more_training_courses = (TextView) view.findViewById(R.id.tv_see_more_training_courses);
        tv_training_courses_data = (TextView) view.findViewById(R.id.tv_training_courses_data);
        tv_see_more_corporate_training = (TextView) view.findViewById(R.id.tv_see_more_corporate_training);
        tv_corporate_training_data = (TextView) view.findViewById(R.id.tv_corporate_training_data);
        tv_see_more_communicationskills = (TextView) view.findViewById(R.id.tv_see_more_communicationskills);
        tv_communicationskills_data = (TextView) view.findViewById(R.id.tv_communicationskills_data);
        tv_see_more_campus_training = (TextView) view.findViewById(R.id.tv_see_more_campus_training);
        tv_campus_training_data = (TextView) view.findViewById(R.id.tv_campus_training_data);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
