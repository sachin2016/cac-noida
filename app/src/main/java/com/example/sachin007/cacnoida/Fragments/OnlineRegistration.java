package com.example.sachin007.cacnoida.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.sachin007.cacnoida.R;

import java.util.ArrayList;

/**
 * Created by sachin007 on 27-09-2016.
 */

public class OnlineRegistration extends Fragment {
    private EditText et_fname;
    private  View view;
    private Spinner sp_course;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         view= inflater.inflate(R.layout.online_registration, container, false);
        findViewById();
        final ArrayList<String> sp=new ArrayList();
        sp.add("Select course !");
        sp.add("Basic .net");
        sp.add("Advance .net");
        sp.add("MVC3 MVC5 MVC");
        sp.add("Angular js and Node js");
        sp.add("Sharedpoint Server 2010");
        ArrayAdapter ad= new ArrayAdapter(getActivity(),R.layout.spinner_layout,sp);
        sp_course.setAdapter(ad);

        return  view;
    }
   private void findViewById(){
       et_fname=(EditText) view.findViewById(R.id.et_fname);
       sp_course=(Spinner) view.findViewById(R.id.sp_course);
   }
}
